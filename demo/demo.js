import '@bbva-web-components/bbva-foundations-theme/bbva-foundations-theme.js';
import './css/demo-styles.js';
import '../cells-comp1-ui.js';
import '@bbva-web-components/bbva-web-form-text/bbva-web-form-text.js';
import '@bbva-web-components/bbva-web-button-default/bbva-web-button-default.js';

// Include below here your components only for demo
// import 'other-component.js'
