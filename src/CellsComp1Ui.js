import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles } from '@bbva-web-components/bbva-core-lit-helpers';
import styles from './CellsComp1Ui-styles.js';
import estilos from './misEstilos';
/**
![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)

This component ...

Example:

```html
<cells-comp1-ui></cells-comp1-ui>
```

##styling-doc

@customElement cells-comp1-ui
*/
export class CellsComp1Ui extends LitElement {
  static get is() {
    return 'cells-comp1-ui';
  }

  // Declare properties
  static get properties() {
    return {
      name: { type: String, },
      variable1: { type: String,},
      total: { type: String,},
      variable2: { type: String,},
      historia: { type: String,},
      signo: { type: String,},
      ope: { type: Boolean,},
      bucle: { type: Boolean,},
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.name = 'Cells';
    this.variable1='0';
    this.signo='0';
    this.historia=0;
    this.variable2=0;
    this.total=0;
    this.ope=false;
    this.bucle=false;
  }

  static get styles() {
    return [
      styles,estilos,
      getComponentSharedStyles('cells-comp1-ui-shared-styles','misEstilos')
    ];
    
  }

  // Define a template
  render() {
    return html`
      <slot></slot>
      
      
        <!--
        <p>Welcome to ${this.name}</p> -->
        <p>${this.historia}</p>
        <bbva-web-form-text id="caja" @change=${this.setVariable1} value=${this.total} readonly="" style="color:red;"></bbva-web-form-text> 
      <div class="a_100 ">
        <div class="a_100 container" >
          <bbva-web-button-default class="a_24 red radio" @click=${this.proximamente}>%</bbva-web-button-default>
          <bbva-web-button-default class="a_24 red radio" @click=${this.resetear}>CE</bbva-web-button-default>
          <bbva-web-button-default class="a_24 red radio" @click=${this.resetear}>C</bbva-web-button-default>
          <bbva-web-button-default class="a_24 red radio">←</bbva-web-button-default>

        </div>

        <div class="a_100 container" >
          <bbva-web-button-default class="a_24 negro radio" @click=${this.siete}>7</bbva-web-button-default>
          <bbva-web-button-default class="a_24 negro radio" @click=${this.ocho}>8</bbva-web-button-default>
          <bbva-web-button-default class="a_24 negro radio" @click=${this.nueve}>9</bbva-web-button-default>
          <bbva-web-button-default class="a_24 red radio" @click=${this.dividir}>/</bbva-web-button-default>

        </div>

        <div class="a_100 container" >
          <bbva-web-button-default class="a_24 negro radio" @click=${this.cuatro}>4</bbva-web-button-default>
          <bbva-web-button-default class="a_24 negro radio" @click=${this.cinco}>5</bbva-web-button-default>
          <bbva-web-button-default class="a_24 negro radio" @click=${this.seis}>6</bbva-web-button-default>
          <bbva-web-button-default class="a_24 red radio" @click=${this.multiplicar}>X</bbva-web-button-default>

        </div>

        <div class="a_100 container" >
          <bbva-web-button-default class="a_24 negro radio" @click=${this.uno}>1</bbva-web-button-default>
          <bbva-web-button-default class="a_24 negro radio" @click=${this.dos}>2</bbva-web-button-default>
          <bbva-web-button-default class="a_24 negro radio" @click=${this.tres}>3</bbva-web-button-default>
          <bbva-web-button-default class="a_24 red radio" @click=${this.restar}>_</bbva-web-button-default>

        </div>

        <div class="a_100 container" >
          <bbva-web-button-default class="a_24 negro radio" @click=${this.invertir}>+/-</bbva-web-button-default>
          <bbva-web-button-default class="a_24 negro radio" @click=${this.cero}>0</bbva-web-button-default>
          <bbva-web-button-default class="a_24 negro radio">.</bbva-web-button-default>
          <bbva-web-button-default class="a_24 red radio" @click=${this.sumar}>+</bbva-web-button-default>

        </div>
        <bbva-web-button-default class="a_100  radio" @click=${this.calcular}>=</bbva-web-button-default>

      </div>
      

    `;
  }
  calcular(){
    let resultado;
    if(this.ope){
      this.variable1=this.total;
    }
    if(!this.bucle){
      this.variable1=this.total;
      this.bucle=true;
     
    }
    if(this.signo=="+"){
      this.historia= this.variable2 + " + " + this.variable1 + " = ";
      resultado = parseFloat(this.variable2) + parseFloat(this.variable1);
      this.variable2 = resultado;
    }else if(this.signo=="-"){
      this.historia= this.variable2 + " - " + this.variable1 + " = ";
      resultado = parseFloat(this.variable2) - parseFloat(this.variable1);
      this.variable2 = resultado;
    }else if(this.signo=="x"){
      this.historia= this.variable2 + " x " + this.variable1 + " = ";
      resultado = parseFloat(this.variable2) * parseFloat(this.variable1);
      this.variable2 = resultado;
    }else if(this.signo=="/"){
      this.historia= this.variable2 + " / " + this.variable1 + " = ";
      resultado = parseFloat(this.variable2) / parseFloat(this.variable1);
      this.variable2 = resultado;
    }
    
    this.total=resultado;
    
    this.ope=false;
  }
  sumar(){
    if(!this.ope){
      this.variable2= this.total;
      this.historia= this.variable2 + " + ";
      this.ope=true;
      this.total = "0";
      this.signo="+";
    }
  }
  restar(){
    if(!this.ope){
      this.variable2= this.total;
      this.historia= this.variable2 + " - ";
      this.ope=true;
      this.total = "0";
      this.signo="-";
    }
  }
  multiplicar(){
    if(!this.ope){
      this.variable2= this.total;
      this.historia= this.variable2 + " x ";
      this.ope=true;
      this.total = "0";
      this.signo="x";
    }
  }
  dividir(){
    if(!this.ope){
      this.variable2= this.total;
      this.historia= this.variable2 + " / ";
      this.ope=true;
      this.total = "0";
      this.signo="/";
    }
  }
  proximamente(){
      this.historia = "Proximamente ... estamos trabajando para brindarte el mejor servicio"
  }

  invertir(){
    this.total = parseFloat(this.total) * -1;
  }
  

  uno(){
    let valor=this.total;
    if(valor=="0"){
      this.total = "1";
    }else{
        this.total = valor + "1";
    }   
  }

  dos(){
    let valor=this.total;
    if(valor=="0"){
      this.total = "2";
    }else{
      this.total = valor + "2";
    }
    
  }
  tres(){
    let valor=this.total;
    if(valor=="0"){
      this.total = "3";
    }else{
      this.total = valor + "3";
    }   
  }
  cuatro(){
    let valor=this.total;
    if(valor=="0"){
      this.total = "4";
    }else{
      this.total = valor + "4";
    }
  }
  cinco(){
    let valor=this.total;
    if(valor=="0"){
      this.total = "5";
    }else{
      this.total = valor + "5";
    }
  }
  seis(){
    let valor=this.total;
    if(valor=="0"){
      this.total = "6";
    }else{
      this.total = valor + "6";
    }
  }
  siete(){
    let valor=this.total;
    if(valor=="0"){
      this.total = "7";
    }else{
      this.total = valor + "7";
    }
  }
  ocho(){
    let valor=this.total;
    if(valor=="0"){
      this.total = "8";
    }else{
      this.total = valor + "8";
    }
  }
  nueve(){
    let valor=this.total;
    if(valor=="0"){
      this.total = "9";
    }else{
      this.total = valor + "9";
    }
  }
  cero(){
    let valor=this.total;
    if(valor=="0"){
      this.total = "0";
    }else{
      this.total = valor + "0";
    }
  }

  resetear(){
    this.variable1='0';
    this.historia=0;
    this.variable2=0;
    this.total=0;
    this.ope=false;
    this.bucle=false;
  }

}
