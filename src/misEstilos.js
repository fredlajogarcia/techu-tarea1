import { css, unsafeCSS } from 'lit-element';

export default css`
    
    .container {
        display:flex;
        flex-flow: row nowrap;
        justify-content:space-evenly;
       
        margin: 2px 0px;
    }
    .a_100{
        width:100%;
    }
    .a_24{
        width:24%;
    }
    .a_33{
        width:33%;
    }
    .negro{
        background-color:black;
    }
    .grey{
        background-color:#474443;
    }
    .red{
        background-color:#AF3C11;
    }
    .radio{
        border-radius:8px;
    }

    
  
  
  `;